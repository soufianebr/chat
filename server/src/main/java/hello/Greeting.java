package hello;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "Greeting")

public class Greeting implements Serializable {

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)@Column(name = "id")
	private  long id;
    private  String content;
    
	public Greeting() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Greeting(long id, String content) {
		super();
		this.id = id;
		this.content = content;
	}

	public Greeting(String content) {
		super();
		this.content = content;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

   

    
}
