package hello;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {
	@Autowired
	UserRepository userRepository;
	@Autowired
	PostRepository postRepository;
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		User u1=userRepository.save(new User("tst", "test@test.com", "test"));
		postRepository.save(new Post(new Date(), "test text", "test titre", u1));
	}
    

	
}
