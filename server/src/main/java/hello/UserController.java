package hello;

import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping(path="/api") 
public class UserController {
	 private static final String template = "Hello, %s!";
	@Autowired
	UserRepository u;
	
	 @CrossOrigin // allow request from all origins
	    @RequestMapping(value="/new-user",
	                    method = RequestMethod.POST)
	    public User newUser(@RequestBody User person) {
	        
	        u.save(person);
	       return  person;
	    }
	 @CrossOrigin
		@GetMapping("/Users")
		List<User> all() {
			return (List<User>) u.findAll();
		}

		

		// Single item
	 @CrossOrigin
		@GetMapping("/Users/{id}")
		Optional<User> one(@PathVariable Long id) {

			return u.findById(id);
				
		}
	 @CrossOrigin
		@PutMapping("/Users/{id}")
		User replaceUser(@RequestBody User newUser, @PathVariable Long id) {

			return u.findById(id)
				.map(User -> {
					User.setUsername(newUser.getUsername());
					User.setPassword(newUser.getPassword());
					User.setEmail(newUser.getEmail());
					return u.save(User);
				})
				.orElseGet(() -> {
					newUser.setId(id);
					return u.save(newUser);
				});
		}
	 @CrossOrigin
		@DeleteMapping("/Users/{id}")
		void deleteUser(@PathVariable Long id) {
			u.deleteById(id);
		}


}
