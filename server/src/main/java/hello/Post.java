package hello;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Post")

public class Post {
	 @Id @GeneratedValue(strategy=GenerationType.IDENTITY)@Column(name = "idpost")
	private Long idpost;
	@Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
	@Type(type="text")
    private String text;
    private String titre;
    @ManyToOne
    private User utilisateur;
    
    
	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Post(Date dateCreation, String text, String titre, User utilisateur) {
		super();
		this.dateCreation = dateCreation;
		this.text = text;
		this.titre = titre;
		this.utilisateur = utilisateur;
	}
	public Long getIdpost() {
		return idpost;
	}
	public void setIdpost(Long idpost) {
		this.idpost = idpost;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public User getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(User utilisateur) {
		this.utilisateur = utilisateur;
	}

}
