package hello;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor

public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	public String username;
	
	public String password;
	
	public String email;
    @OneToMany(mappedBy="utilisateur")  
	public Set<Post> posts;
	    
	    public User(String username, String email,String password) {
	    	
	    	this.username = username;
	    	this.email = email;
	    	this.password = password;
	    }
	    public User(Long id) {
	    	this.id = id;
	    	
	    }
	    public User( String username, String password) {
	    	
	    	this.username = username;
	    	this.password = password;
	    }
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}

}
